#!/bin/sh

set -e

# Run this script in the directory where the zip file is located The
# new tarball will be a .tar.gz file, so no overwriting of the initial
# file is to be worried about.

mkdir -f temp
cd temp
rm -rf *
cp ../$1 .

unzip $1
sourceDir=$(basename $1 .zip)

cd ${sourceDir}

rm -rfv MSToolkit
rm -rfv CometWrapper
rm -rfv CometUI
rm -rfv CometSetup

find -name "*.dll" | xargs rm -v


cd ..

tar cvzf ${sourceDir}.tar.gz ${sourceDir}
echo "Done, creating cleaned source tarball ${sourceDir}.tar.gz"
